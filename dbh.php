<?php

$conn = mysqli_connect("localhost", "root", "1234", "login_test");

#if (!$conn) {
#	die("Connection Failed " . mysqli_connect_error());
#}

if (!$conn) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}